const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 8080;

app.use(express.static('dist'));

app.get('*', function (req, res) {
    res.sendFile(path.resolve(__dirname, 'dist/index.html'));
});

app.listen(port);
console.log('App started s');