import React, {Component} from 'react';
import sortDayNames from '../utils/mapDayNameToPolish'
import '../styles/ShopInfoWindow.scss';

export default class ShopDesc extends Component {

	showHours() {
		return this.props.shop.godziny.map(days => {
				return sortDayNames(days).map(dayHours => {
						return (
							<tr>
								<td>{dayHours.name}</td>
								<td>{dayHours.hours}</td>
							</tr>
						);
					}
				)
				;
			}
		);
	}

	render() {
		return (
			<div>
				<img
					src={this.props.shop.logo}
					className="infoWindow_img"/>
				<p className="infoWindow_name">{this.props.shop.siec}</p>
				<p className="infoWindow_address">{this.props.shop.adres}</p>
				<table className="infoWindow_table">
					<thead>
					<tr>
						<th>Dzień</th>
						<th>Godziny otwarcia</th>
					</tr>
					</thead>
					<tbody>
						{this.showHours()}
					</tbody>
				</table>
			</div>
		);
	}
}