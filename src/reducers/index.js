import { combineReducers } from "redux";
import ShopsReducer from "./reducer_shops";
import ActiveShop from "./reducer_actve_shop";

const rootReducer = combineReducers({
    shops: ShopsReducer,
    activeShop: ActiveShop
});

export default rootReducer;
