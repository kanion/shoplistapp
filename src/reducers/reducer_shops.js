import {FETCH_SHOPS} from './../actions/fetch-shops';

export default function (state = [], action) {
    switch (action.type) {
        case FETCH_SHOPS:
            return action.payload.data;
    }
    return state;
}