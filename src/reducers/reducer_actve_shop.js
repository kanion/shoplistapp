import {SHOP_SELECTED} from './../actions/select-shop';

export default function(state = null, action) {

    switch (action.type) {
        case SHOP_SELECTED:
            return action.payload;
    }

    return state;
}
