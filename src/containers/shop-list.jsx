import React, { Component } from "react";
import { connect } from "react-redux";
import { selectShop } from "../actions/select-shop";
import { bindActionCreators } from "redux";
import pushToDataLayer from "../actions/dataLayer";

import '../styles/ShopList.scss';

class ShopList extends Component {

    selectShop(shop){
	    pushToDataLayer("list-click", shop.siec);
	    this.props.selectShop(shop);
    }

    renderList() {
        if (!this.props.shops) {
            return <div>Lista sklepów pusta</div>;
        }

        return this.props.shops.map(shop => {

            return (
                <li
                    key={shop.adres + shop.siec}
                    onClick={() => this.selectShop(shop)}
                >
                    {shop.adres}
                </li>
            );
        });
    }

    render() {
        return (
            <ul className="shoplist">
                {this.renderList()}
            </ul>
        );
    }
}

function mapStateToProps(state) {
    return {
        shops: state.shops
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ selectShop }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopList);