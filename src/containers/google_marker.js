import React, {Component} from 'react';
import {connect} from 'react-redux';
import {selectShop} from '../actions/select-shop';
import {bindActionCreators} from 'redux';
import pushToDataLayer from '../actions/dataLayer';
import CustomMarker from '../utils/customMarker';
import '../styles/CustomMarker.scss';

class GoogleMarker extends Component {

	constructor(props) {
		super(props);
		this.marker = null;

		this.setMarkerAsActive = this.setMarkerAsActive.bind(this);
	}

	componentDidMount() {
		if (this.props.map && this.props.shop) {
			this.renderMarker();
		}
	}

	renderMarker() {
		const customMarker = new CustomMarker(
			new google.maps.LatLng(this.props.shop.latitude, this.props.shop.longitude),
			this.props.map,
			{
				shop: this.props.shop,
				clickEvent: this.setMarkerAsActive
			}
		);
	}

	setMarkerAsActive() {
		pushToDataLayer('marker-click', this.props.shop.siec);
		this.props.selectShop(this.props.shop);
	}

	render() {
		return null;
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({selectShop}, dispatch);
}

export default connect(null, mapDispatchToProps)(GoogleMarker);