import React, {Component} from "react";
import ReactDOM from 'react-dom';

import {connect} from "react-redux";

import ShopDesc from '../components/shop_desc';

class InfoWindow extends Component {

    constructor(props) {
        super(props);

        this.infowindow = new google.maps.InfoWindow({
            content: ''
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props.activeShop !== prevProps.activeShop) {
            this.renderInfoWindow();
        }
    }

    renderInfoWindow() {
        if (this.props.activeShop) {
            const div = document.createElement('div');

            ReactDOM.render(<ShopDesc shop={this.props.activeShop}/>, div)
            this.infowindow.setContent(div);
            this.infowindow.setPosition(new google.maps.LatLng(this.props.activeShop.latitude, this.props.activeShop.longitude));
            this.infowindow.open(this.props.map);
        }
    }

    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
        activeShop: state.activeShop
    };
}

export default connect(mapStateToProps)(InfoWindow);