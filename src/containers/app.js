import React from 'react';
import styles from '../styles/App.scss';
import ShopDetail from "./shop-detail.jsx";
import ShopList from "./shop-list.jsx";

import { connect } from "react-redux";
import { fetchShops } from "../actions/fetch-shops";
import { bindActionCreators } from "redux";
import GoogleMap from "./google_map";
import GoogleMarker from "./google_marker";
import InfoWindow from "./info_window";

class App extends React.Component {
    constructor(props){
        super(props);

        this.renderShopsMarkers = this.renderShopsMarkers.bind(this);

        this.props.fetchShops();
    }

    renderShopsMarkers(shop){
        return (
            <GoogleMarker shop={shop} key={shop.adres + shop.siec}/>
        );
    }

    render() {

        if(!this.props.shops) return (
            <div>Pobieranie listy sklepow</div>
        );

        return (
            <div>
                <GoogleMap>
                    {this.props.shops.map(this.renderShopsMarkers)}
                    <InfoWindow/>
                </GoogleMap>
                <ShopDetail />
                <ShopList />
            </div>);
    }
}

function mapStateToProps(state) {
    return {
        shops: state.shops
    };
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchShops }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
