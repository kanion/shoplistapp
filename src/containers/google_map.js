import React, {Component} from "react";
import {connect} from "react-redux";

import '../styles/Map.scss';

class GoogleMap extends Component {

    constructor(props){
        super(props);

        this.map = null;
    }

    renderMap() {
        if (!this.props.activeShop && navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((pos) => {
                const coords = pos.coords;
                this.map.setCenter(new google.maps.LatLng(coords.latitude, coords.longitude));

            });
        } else if (this.props.activeShop) {
            this.map.setCenter(new google.maps.LatLng(this.props.activeShop.latitude, this.props.activeShop.longitude));
        }
    }

    componentDidUpdate() {
        this.renderMap();
    }

    componentDidMount() {
        this.map = new google.maps.Map(this.refs.map, {
            zoom: 15
        });
        this.renderMap();
    }

    renderChildren() {
        const {children} = this.props;

        if (!children) return;

        return React.Children.map(children, c => {
            return React.cloneElement(c, {
                map: this.map,
                shop: c.props.shop
            });
        })
    }

    render() {
        return (
            <div ref="map" className="map">
                {this.renderChildren()}
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        activeShop: state.activeShop
    };
}

export default connect(mapStateToProps)(GoogleMap);