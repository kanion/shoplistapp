import React, { Component } from "react";
import { connect } from "react-redux";
import '../styles/ShopDetails.scss';

class ShopDetail extends Component {
    render() {
        if (!this.props.shop) {
            return <div className="shopdetails">Wybierz sklep z listy</div>;
        }

        return (
            <div className="shopdetails">
                <h3 className="shopdetails_header">Szczegóły sklepu:</h3>
                <div className="shopdetails_description">Address: {this.props.shop.adres}</div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        shop: state.activeShop
    };
}

export default connect(mapStateToProps)(ShopDetail);
