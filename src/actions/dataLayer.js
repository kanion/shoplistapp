export default function pushToDataLayer(eventName, shopName) {
	window.dataLayer = window.dataLayer || [];
	window.dataLayer.push({
		'event': eventName,
		'shop-name': shopName
	});
}