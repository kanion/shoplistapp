import axios from "axios";

const URL = "https://gist.githubusercontent.com/kanion/fa33a95e2aea7a84db8e8b9e79de06e1/raw/3dc9640c3908e4fdfe3b957412f0ae18b6aa233f/shoplist";

export const FETCH_SHOPS = "FETCH_SHOPS";

export function fetchShops() {
    const request = axios.get(URL);

    return {
        type: FETCH_SHOPS,
        payload: request
    };
}

