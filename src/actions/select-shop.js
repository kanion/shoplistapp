export const SHOP_SELECTED = "SHOP_SELECTED";

export function selectShop(shop) {
    return {
        type: "SHOP_SELECTED",
        payload: shop
    };
}