function mapDayNameToPolish(dayName) {
	switch (dayName) {
		case 'Monday':
			return 'Poniedziałek';
			break;
		case 'Tuesday':
			return 'Wtorek';
			break;
		case 'Wednesday':
			return 'Środa';
			break;
		case 'Thursday':
			return 'Czwartek';
			break;
		case 'Friday':
			return 'Piątek';
			break;
		case 'Saturday':
			return 'Sobota';
			break;
		case 'Sunday':
			return 'Niedziela';
			break;
		default:
			return '';
	}
}

export default function sortDayNames(dayNames) {

	const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
	let sorted = [];

	Object.keys(dayNames).map((day)=>{
		sorted[days.indexOf(day)] = { name: mapDayNameToPolish(day), hours: dayNames[day] };
	});
	return sorted;
}