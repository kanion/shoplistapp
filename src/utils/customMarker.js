function CustomMarker(latlng, map, args) {
	this.latlng = latlng;
	this.args = args;
	this.setMap(map);
}

CustomMarker.prototype = new google.maps.OverlayView();

CustomMarker.prototype.draw = function () {

	let self = this,
		div = this.div;

	if (!div) {

		div = this.div = document.createElement('div');
		div.innerHTML = `<img src='${this.args.shop.logo}' class='tooltip_image'/>`;
		div.className = 'tooltip';

		google.maps.event.addDomListener(div, 'click', function (event) {

			self.args.clickEvent();
			google.maps.event.trigger(self, 'click');
		});

		let panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}

	let point = this.getProjection().fromLatLngToDivPixel(this.latlng);

	if (point) {
		div.style.left = (point.x - 20) + 'px';
		div.style.top = (point.y - 40) + 'px';
	}
};

CustomMarker.prototype.remove = function () {
	if (this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}
};

CustomMarker.prototype.getPosition = function () {
	return this.latlng;
};

export default CustomMarker;